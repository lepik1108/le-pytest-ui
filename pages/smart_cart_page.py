from selenium.webdriver.common.by import By
from utils.logs.logger import logger
from pages.base_page import BasePage
from pages.cart_page import CartPage


class SmartCartPage(BasePage):
    CART_AMOUNT = (By.CSS_SELECTOR, '.a-size-base.a-color-price.a-text-bold')
    PRODUCT_COUNT = (By.ID, 'nav-cart-count')
    ADDED_MESSAGE = (By.CLASS_NAME, 'a-size-medium-plus')
    ADDED_SUCCESS_ICON = (By.CLASS_NAME, 'a-alert-inline-success')
    GO_TO_CART = (By.XPATH, '//a[contains(text(), "Go to Cart") and @class="a-button-text"]')

    def add_to_cart_succeed(self):
        success_icon = self.ADDED_SUCCESS_ICON
        message = self.wait_for_element(self.ADDED_MESSAGE)
        if not success_icon or message.text != 'Added to Cart':
            message = 'Failed to add product to cart'
            logger.error(message)
            raise Exception(message)

    @property
    def product_count(self):
        count = self.element_displayed(self.PRODUCT_COUNT)
        return count.text

    @property
    def cart_amount(self):
        amount = self.element_displayed(self.CART_AMOUNT)
        return amount.text[1:]

    def go_to_cart(self):
        self.click(self.GO_TO_CART)
        return CartPage(self.driver)
