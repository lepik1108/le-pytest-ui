from selenium.webdriver.common.by import By
from utils.logs.logger import logger
from pages.base_page import BasePage
from pages.smart_cart_page import SmartCartPage


class ProductPage(BasePage):
    ADD_TO_CART_BUTTON = (By.ID, 'add-to-cart-button')
    CART_COUNT = (By.ID, 'nav-cart-count')
    PRODUCT_TITLE = (By.ID, 'productTitle')
    PRODUCT_COUNT = (By.ID, 'nav-cart-count')
    PRODUCT_PRICE_USD = (By.CLASS_NAME, 'a-price-whole')
    PRODUCT_PRICE_CENTS = (By.CLASS_NAME, 'a-price-fraction')

    def add_to_cart(self):
        self.js_click(self.ADD_TO_CART_BUTTON)
        logger.info(f'Product have been added to cart')
        return SmartCartPage(self.driver)

    @property
    def product_title(self):
        title = self.element_displayed(self.PRODUCT_TITLE)
        return title.text

    @property
    def product_count(self):
        count = self.element_displayed(self.PRODUCT_COUNT)
        return count.text

    @property
    def product_price(self):
        price_usd = self.element_displayed(self.PRODUCT_PRICE_USD)
        price_cents = self.element_displayed(self.PRODUCT_PRICE_CENTS)
        return f'%s.%s' % (price_usd.text, price_cents.text)
