from selenium.webdriver.common.by import By
from utils.logs.logger import logger
from pages.base_page import BasePage
from pages.product_page import ProductPage


class SearchPage(BasePage):
    CART_AMOUNT = (By.CSS_SELECTOR, '.a-color-price')
    PRODUCT_COUNT = (By.CSS_SELECTOR, 'nav-cart-count')
    TITLES = (
        By.CSS_SELECTOR,
        f'a[class="a-link-normal s-underline-text '
        f's-underline-link-text s-link-style a-text-normal"]'
    )
    PRICES = (By.XPATH, './/*[@class="a-offscreen"]')
    RATES = (By.XPATH, './/*[@class="a-icon-alt"]')

    def results(self):
        titles = [e for e in self.driver.find_elements(*self.TITLES)]
        # prices = [e for e in self.driver.find_elements(*self.PRICES)]
        # rates = [e for e in self.driver.find_elements(*self.RATES)]
        if not titles:
            message = 'There are no results matching the search criteria'
            logger.error(message)
            raise Exception(message)
        # return [(t, p, r)
        #         for t in titles
        #         for p in prices
        #         for r in rates]

    def click_first_item(self):
        search_results = self.driver.find_elements(*self.TITLES)
        first = search_results[0]
        self.click(first)
        logger.info('Clicking the 1st product')
        return ProductPage(self.driver)

