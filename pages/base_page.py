import time
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions
from selenium.common.exceptions import TimeoutException, WebDriverException
from utils.logs.logger import logger
from utils.tools.attachments import attach_png


class BasePage:

    def __init__(self, driver):
        self.driver = driver

    @property
    def page_title(self):
        logger.info(f'Page title is "{self.driver.title}"')
        return self.driver.title

    @property
    def page_url(self):
        logger.info(f'Page URL is "{self.driver.current_url}"')
        return self.driver.current_url

    def go_to(self, url):
        self.driver.get(url)

    def back(self):
        self.driver.back()

    def forward(self):
        self.driver.forward()

    def refresh(self):
        self.driver.refresh()

    def element_displayed(self, element):
        try:
            return WebDriverWait(self.driver, 15)\
                .until(expected_conditions.visibility_of_element_located(element))
        except Exception:
            message = f'Element {element} is not displayed'
            logger.exception(message)
            raise Exception(message)

    def wait_for_element(self, element):
        try:
            return WebDriverWait(self.driver, 15)\
                .until(expected_conditions.element_to_be_clickable(element))
        except Exception:
            message = f'Element {element} was not found'
            logger.exception(message)
            raise Exception(message)

    def click(self, element):
        try:
            element = self.wait_for_element(element)
            element.click()
            logger.info(f'Clicked on element {element}')
        except Exception:
            message = f'Clicking on element {element} failed'
            logger.exception(message)
            raise Exception(message)

    def js_click(self, element):
        web_element = self.wait_for_element(element)
        try:
            self.driver.execute_script('arguments[0].click();', web_element)
            logger.info(f'Clicked on element using javascript {element}')
        except Exception:
            message = f'js clicking on element {element} failed'
            logger.exception(message)
            raise Exception(message)

    def type(self, element, text):
        try:
            self.wait_for_element(element).send_keys(text)
            logger.info(f'Typed {text} in element {element}')
        except Exception:
            message = f'Typing in element {element} failed'
            logger.exception(message)
            raise Exception(message)

    def take_screenshot(self):
        prefix = f'{int(time.time())}'
        attach_png(self.driver.get_screenshot_as_png(), f'screenshot_{prefix}')
        logger.info(f'Screenshot was taken for test {prefix}')

    def page_loaded(self):
        timeout = 15
        try:
            login_page_ready = (
                """
                var ready = document.readyState == 'complete' &&
                            jQuery.active == 0;
                return ready;
                """)
            self.driver.execute_script(login_page_ready)
        except WebDriverException:
            self.driver.refresh()
            raise WebDriverException(
                "login_page_ready script does not executed successfully"
            )
        except TimeoutException:
            self.driver.refresh()
            raise TimeoutException(
                "page doesn't load in %(timeout)s" % locals()
            )
