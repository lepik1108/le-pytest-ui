import re
from selenium.webdriver.common.by import By
from utils.logs.logger import logger
from pages.base_page import BasePage


class CartPage(BasePage):
    PRODUCT_PRICE = (By.CLASS_NAME, 'sc-product-price')
    PRODUCT_COUNT_DROPDOWN = (By.CLASS_NAME, 'a-dropdown-prompt')
    PRODUCT_COUNT_CART = (By.ID, 'sc-subtotal-label-activecart')
    PRODUCT_COUNT_BUY_BOX = (By.ID, 'sc-subtotal-label-buybox')
    CART_AMOUNT = (By.ID, 'sc-subtotal-amount-activecart')
    CART_AMOUNT_BUY_BOX = (By.ID, 'sc-subtotal-amount-buybox')

    def verify_cart(self):
        prices = [
            self.element_displayed(self.PRODUCT_PRICE).text.strip(),
            self.element_displayed(self.CART_AMOUNT).text.strip(),
            self.element_displayed(self.CART_AMOUNT_BUY_BOX).text.strip()
        ]
        cart_count = self.element_displayed(self.PRODUCT_COUNT_CART).text
        buybox_count = self.element_displayed(self.PRODUCT_COUNT_BUY_BOX).text
        cart_count = re.search('Subtotal \((.*) item.*', cart_count)
        buybox_count = re.search('Subtotal \((.*) item.*', buybox_count)
        cart_count = cart_count.group(1)
        buybox_count = buybox_count.group(1)
        counts = [
            self.element_displayed(self.PRODUCT_COUNT_DROPDOWN).text,
            cart_count,
            buybox_count,
        ]
        prices_match = all(p == prices[0] for p in prices)
        counts_match = all(c == counts[0] for c in counts)
        if not prices_match:
            message = 'Failed to verify prices for cart'
            logger.error(message)
            raise Exception(message)
        elif not counts_match:
            message = 'Failed to verify cart items count'
            logger.error(message)
            raise Exception(message)
        else:
            logger.info(f'Cart item added successfully')
