from selenium.webdriver.common.by import By
from pages.base_page import BasePage
from pages.search_page import SearchPage


class HomePage(BasePage):
    SEARCH_INPUT = (By.NAME, 'field-keywords')
    SEARCH_BUTTON = (By.CSS_SELECTOR, 'input[value="Go"]')
    PRODUCT_COUNT = (By.ID, 'nav-cart-count')

    def search_items(self, text):
        self.click(self.SEARCH_INPUT)
        self.type(self.SEARCH_INPUT, text)
        self.click(self.SEARCH_BUTTON)
        return SearchPage(self.driver)
