# le-pytest-ui

## ** Framework contains:**

- Cross-browser support for running tests:
    - Running tests locally using webdriver-manager
    - Running tests on Docker containers using Selenoid

- Proxy pattern usage for both local/remote chrome with selenium-wide
  
- Support for parallel execution
  
- Platform and browser parameterization using env variabless

- Tests run report and dashboard using Allure (including screenshots)

- Test data management from external file

- Logger (including errors, traceback, test steps, etc.)

- Page Object Model design

## Installation

```sh
$ cd le-pytest-ui
$ pip3 install -r requirements.txt --user
```

Setup selenoid manually on hosts:

-on linux/macos
 - using aerokube/cm with docker
 ```sh
 $ curl -s https://aerokube.com/cm/bash | bash && rm ~/.aerokube/selenoid/browsers.json && ./cm selenoid configure --vnc --last-versions 2 --config-dir ./ && ./cm selenoid start --vnc --config-dir ./ && ./cm selenoid-ui update
 ```

-on Windows
 - using aerokube/cm with docker
 ```sh
 $ curl -s https://aerokube.com/cm/bash | bash && rm ~/.aerokube/selenoid/browsers.json && ./cm selenoid configure --vnc --last-versions 2 --config-dir ./ && ./cm selenoid start --vnc --config-dir ./ && ./cm selenoid-ui update
 ```

### Running tests locally
Run test on your local host's chrome(not container) with saving all reports:
```sh
pytest tests
```

Run test on your local host's 3 browsers (containers) with saving all reports:
```sh
PLATFORM=remote && BROWSER=chrome,firefox,opera && pytest tests
```

Run generate allure reports:
```sh
allure serve reports/allure
```