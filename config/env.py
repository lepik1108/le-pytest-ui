import os
import socket

# sets a host ip address variable
# assumes you have an internet access, and that there is no local proxy
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.connect(("8.8.8.8", 80))
ip = s.getsockname()[0]
s.close()

variables = {
    'browsers': os.environ.get('BROWSERS', 'chrome').split(','),
    'platform': os.environ.get('PLATFORM', 'local'),
    'remoteUrl': os.environ.get('GRID_URL', 'http://localhost:4444/wd/hub'),
    'proxy': eval(os.environ.get('PROXY', 'False')),
}
