from enum import Enum


class SupportedPlatforms(Enum):
    LOCAL = 1
    REMOTE = 2
