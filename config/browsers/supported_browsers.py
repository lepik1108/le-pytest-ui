from enum import Enum


class SupportedBrowsers(Enum):
    chrome = 1
    firefox = 2
    opera = 3
    safari = 4
    edge = 5
    ie = 6
