from config.browsers.browser import Browser
from webdriver_manager.chrome import ChromeDriverManager
from config.env import ip, variables


class ChromeBrowser(Browser):
    @classmethod
    def init_browser(cls):
        x = variables['proxy']
        if variables['proxy']:
            from seleniumwire import webdriver
            options = {
                'addr': ip,
                'request_storage': 'memory',
                'request_storage_base_dir': 'proxy'
            }
            return webdriver.Chrome(
                executable_path=ChromeDriverManager().install(),
                seleniumwire_options=options
            )
        else:
            from selenium import webdriver
            return webdriver.Chrome(
                executable_path=ChromeDriverManager().install(),
            )
