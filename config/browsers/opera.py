from config.browsers.browser import Browser
from selenium import webdriver
from webdriver_manager.opera import OperaDriverManager


class OperaBrowser(Browser):
    @classmethod
    def init_browser(cls):
        options = webdriver.ChromeOptions()
        options.add_argument('allow-elevated-browser')
        options.add_experimental_option('w3c', True)
        options.binary_location = "/usr/bin/opera"
        return webdriver.Opera(executable_path=OperaDriverManager().install(), options=options)
