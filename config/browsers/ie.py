from config.browsers.browser import Browser
from selenium import webdriver
from selenium.webdriver.ie.service import Service as IEService
from webdriver_manager.microsoft import IEDriverManager


class IEBrowser(Browser):
    @classmethod
    def init_browser(cls):
        return webdriver.Ie(service=IEService(IEDriverManager().install()))
