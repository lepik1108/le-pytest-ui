from config.browsers.chrome import ChromeBrowser
from config.browsers.firefox import FirefoxBrowser
from config.browsers.opera import OperaBrowser
from config.browsers.safari import SafariBrowser
from config.browsers.ie import IEBrowser
from config.browsers.edge import EdgeBrowser
from config.browsers.supported_browsers import SupportedBrowsers


class BrowserFactory:
    @staticmethod
    def get_browser(config_browser):
        if config_browser == SupportedBrowsers.chrome.name:
            return ChromeBrowser()
        if config_browser == SupportedBrowsers.firefox.name:
            return FirefoxBrowser()
        if config_browser == SupportedBrowsers.opera.name:
            return OperaBrowser()
        if config_browser == SupportedBrowsers.safari.name:
            return SafariBrowser()
        if config_browser == SupportedBrowsers.edge.name:
            return EdgeBrowser()
        if config_browser == SupportedBrowsers.ie.name:
            return IEBrowser()
