from config.browsers.browser import Browser
from selenium import webdriver


class SafariBrowser(Browser):
    @classmethod
    def init_browser(cls):
        return webdriver.Safari()
