import time
import requests
import decorator
from requests import ConnectionError
from selenium.common.exceptions import WebDriverException, TimeoutException


def retry(f=None, tries=4, delay=3, backoff=2,
          exceptions=(
                  ConnectionError,
                  requests.exceptions.ConnectionError,
                  WebDriverException,
                  TimeoutException
          ),
          logger=None):
    """
    Retry calling the decorated function using an exponential backoff.
    Args:
        exceptions: The exception to check. may be a tuple of
            exceptions to check.
        tries: Number of times to try (not retry) before giving up.
        delay: Initial delay between retries in seconds.
        backoff: Backoff multiplier (e.g. value of 2 will double the delay
            each retry).
        logger: Logger to use. If None, print.
    """
    def wrapper(f, *args, **kwargs):
        mtries, mdelay = tries, delay
        while mtries > 1:
            try:
                return f(*args, **kwargs)
            except exceptions as e:
                msg = '{}, Retrying in {} seconds...'.format(e, mdelay)
                if logger:
                    logger.warning(msg)
                else:
                    print(msg)
                time.sleep(mdelay)
                mtries -= 1
                mdelay *= backoff
        return f(*args, **kwargs)

    return decorator.decorator(wrapper, f)
