import sys
import traceback
import decorator
from utils.tools.attachments import attach_png
from utils.logs.logger import logger


def screenshot(func=None):
    """ Decorator function which takes screenshot at Allure step.
        Decorate tests with you want to produce screenshots
    """
    def wraps(*args, **kwargs):
        # result = None
        try:
            driver = request = None
            for arg in args:
                if 'webdriver' in f'{type(arg)}':
                    driver = arg
                if 'FixtureRequest' in f'{type(arg)}':
                    request = arg
            if driver is None:
                error = f"Screenshot decorator requires function to have argument: driver"
                logger.error(error)
                raise Exception(error)
            if request is None:
                error = f"Screenshot decorator requires function to have argument: request"
                logger.error(error)
                raise Exception(error)

            attach_png(driver.get_screenshot_as_png(), f"screenshot_{request.node.name}")
            logger.info(f"Screenshot was taken for test {request.node.name}")
        except Exception:
            _type, _value, _traceback = sys.exc_info()
            raise _type(''.join(traceback.format_exception(_type, _value, _traceback)))
        # return result

    return decorator.decorator(wraps, func)
