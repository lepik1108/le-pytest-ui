import allure


def attach_plain_text(attachment, attachment_name="TEXT attachment"):
    """Attach TEXT/PLAIN string to report"""
    allure.attach(str(attachment), attachment_name, allure.attachment_type.TEXT)


def attach_png(attachment, attachment_name="Screenshot"):
    """Attach IMAGE/PNG to report"""
    allure.attach(attachment, attachment_name, allure.attachment_type.PNG)
