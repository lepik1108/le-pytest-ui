import allure
import pytest
import time
from utils.logs.logger import logger
from config.env import variables, ip
from config.browsers.browser_factory import BrowserFactory
from config.browsers.supported_browsers import SupportedBrowsers
from config.platforms.supported_platforms import SupportedPlatforms
from tests.test_base import TestBase


@pytest.fixture(scope="session")
def data():
    """Fixture for storing data available for each test.
    Extends itself with test variables stored as object attributes.
    Basically stores pages.

    Returns:
        object -- data object.
    """
    data = type("data", (), {})()
    return data


@pytest.fixture(scope='session', autouse=True)
def env_variables():
    return variables


@pytest.fixture(scope='session')
def config_platform(env_variables):
    # Validate and return the platform choice (grid or local) from the config file
    if 'platform' not in env_variables:
        error = 'The config file does not contain "platform"'
        logger.error(error)
        raise Exception(error)
    elif env_variables['platform'].upper() not in SupportedPlatforms.__members__:
        error = f'"{env_variables["platform"]}" is not a supported platform'
        logger.error(error)
        raise Exception(error)
    logger.info(f'{env_variables["platform"]} platform configured')
    return env_variables['platform']


@pytest.fixture(scope='session')
def config_browsers(env_variables):
    # Fixture validates browsers
    browsers = env_variables["browsers"]
    if 'browsers' not in env_variables:
        error = 'The config file does not contain "browsers"'
        logger.error(error)
        raise Exception(error)
    elif all(b in browsers for b in SupportedBrowsers.__members__):
        error = f'"{browsers}" are not supported'
        logger.error(error)
        raise Exception(error)
    logger.info(f"{browsers} browsers configured")
    return browsers


@pytest.fixture(params=variables['browsers'], scope='class')
def driver(env_variables, config_platform, config_browsers, request):
    browser = request.param
    if config_platform.upper() == SupportedPlatforms.LOCAL.name:
        browser = BrowserFactory.get_browser(browser)
        driver = browser.init_browser()
    else:
        capabilities = {
            "browserName": browser,
            "enableVNC": True,
            "selenoid:options": {
                "enableVNC": True,
            }
        }
        if variables['proxy']:
            from seleniumwire import webdriver
            options = {
                'addr': ip,
                'request_storage': 'memory'
            }
            driver = webdriver.Remote(
                command_executor=env_variables["remoteUrl"],
                desired_capabilities=capabilities,
                seleniumwire_options=options,
            )
        else:
            from selenium import webdriver
            driver = webdriver.Remote(
                command_executor=env_variables["remoteUrl"],
                desired_capabilities=capabilities,
            )

    logger.info("Driver initialized")
    driver.maximize_window()
    logger.info("Browser's window maximized")
    driver.get(TestBase.get_data('baseUrl'))
    logger.info(f"Navigated to {TestBase.get_data('baseUrl')}")

    yield driver

    # if driver:
    #     if request.session['testfailed'] == 1:
    #         allure.attach(driver.get_screenshot_as_png(), name=f"screenshot_{request.node.name}")
    #         logger.info(f"Screenshot was taken for test {request.node.name}")

    allure.attach(driver.get_screenshot_as_png(), name=f"screenshot_failed_{int(time.time())}")
    logger.info(f"Screenshot was taken for test {request.node.name}")
    driver.quit()
    logger.info("Driver was closed")


@pytest.hookimpl(tryfirst=True, hookwrapper=True)
def pytest_runtest_makereport(item, call):
    # execute all other hooks to obtain the report object
    outcome = yield
    rep = outcome.get_result()
    # set a report attribute for each phase of a call, can be "setup", "call", "teardown"
    setattr(item, "rep_" + rep.when, rep)
