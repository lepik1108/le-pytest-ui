from pages.home_page import HomePage
from tests.test_base import TestBase
import allure


class TestA(TestBase):
    URL = TestBase.get_data('baseUrl')
    SEARCH_ITEM = TestBase.get_data('searchItem')

    @allure.severity(allure.severity_level.TRIVIAL)
    @allure.description('Open url www.amazon.com')
    def test_open_url(self, data, driver, request):
        home = HomePage(driver)
        actual_url = home.page_url
        home.take_screenshot()
        data.home = home
        assert actual_url == self.URL, 'URL is incorrect'

    @allure.severity(allure.severity_level.TRIVIAL)
    @allure.description('Search for "software testing"')
    def test_search(self, data, driver, request):
        search = data.home.search_items(self.SEARCH_ITEM)
        search.results()
        search.take_screenshot()
        data.search_results = search.results()
        data.search = search

    @allure.severity(allure.severity_level.TRIVIAL)
    @allure.description('Click the first item')
    def test_click_first(self, data, driver, request):
        data.product = data.search.click_first_item()
        data.product.take_screenshot()

    @allure.severity(allure.severity_level.TRIVIAL)
    @allure.description('Add item to cart')
    def test_add_item(self, data, driver, request):
        smart_cart = data.product.add_to_cart()
        smart_cart.add_to_cart_succeed()
        cart_count = smart_cart.product_count
        cart_amount = smart_cart.cart_amount
        cart_count_increased = int(cart_count) == int(data.product.product_count)
        cart_amount_correct = (cart_amount == data.product.product_price)
        result = cart_count_increased and cart_amount_correct
        smart_cart.take_screenshot()
        data.smart_cart = smart_cart
        assert result, 'Cart is invalid'

    @allure.severity(allure.severity_level.TRIVIAL)
    @allure.description('Open the cart')
    def test_open_cart(self, data, driver, request):
        cart = data.smart_cart.go_to_cart()
        cart.verify_cart()
        cart.take_screenshot()
